package com.sc.security.dto;

import com.google.common.collect.Lists;
import com.sc.security.model.SysAclModule;
import com.sc.security.model.SysDept;
import lombok.Data;
import lombok.ToString;
import org.springframework.beans.BeanUtils;

import java.util.List;

@Data
@ToString
public class AclModuleDto extends SysAclModule {
    private List<AclModuleDto> aclModuleList = Lists.newArrayList();
    private List<AclDto> aclList = Lists.newArrayList();
    public static AclModuleDto adapt(SysAclModule module){
        AclModuleDto dto = new AclModuleDto();
        BeanUtils.copyProperties(module,dto);
        return dto;
    }
}
