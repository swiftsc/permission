package com.sc.security.dto;

import com.google.common.collect.Lists;
import com.sc.security.model.SysRole;
import lombok.Data;
import org.springframework.beans.BeanUtils;
import java.util.List;

@Data
public class RoleDto extends SysRole{
    private List<RoleDto> aclModuleList = Lists.newArrayList();

    public static RoleDto adapt(SysRole role){
        RoleDto dto = new RoleDto();
        BeanUtils.copyProperties(role,dto);
        return dto;
    }
}
