package com.sc.security.dto;


import lombok.Data;

import java.util.Date;

@Data
public class LogDto {
    private Integer type;
    private String beforeSeg;
    private String afterSeg;
    private String operator;
    private Date fromTime;//yyyy-MM-dd
    private Date toTime;
}
