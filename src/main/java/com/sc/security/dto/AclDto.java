package com.sc.security.dto;

import com.sc.security.model.SysAcl;
import lombok.Data;
import lombok.ToString;
import org.springframework.beans.BeanUtils;

@Data
@ToString
public class AclDto extends SysAcl{
    //是否选中
    private boolean checked=false;
    //是否有权限操作
    private boolean hasAcl = false;
    public static AclDto adapt(SysAcl acl){
        AclDto dto = new AclDto();
        BeanUtils.copyProperties(acl,dto);
        return dto;
    }
}
