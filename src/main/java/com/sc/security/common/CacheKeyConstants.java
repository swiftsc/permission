package com.sc.security.common;


import lombok.Getter;

@Getter
public enum CacheKeyConstants {
    SYSTEM_ACLS,
    USER_ACLS;
}
