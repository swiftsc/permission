package com.sc.security.controller;

import com.sc.security.common.JsonData;
import com.sc.security.service.ModifyUserInfoService;
import com.sc.security.vo.ModifyUserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/sys/profile")
public class ProfileController {

    @Autowired
    private ModifyUserInfoService modifyUserInfoService;

    @RequestMapping("/index.page")
    public ModelAndView index()
    {
        return new ModelAndView("profile");
    }

    @RequestMapping("/modify.json")
    @ResponseBody
    public JsonData modify(ModifyUserVo userVo){
        modifyUserInfoService.change(userVo);
        return JsonData.success();
    }
}
