package com.sc.security.controller;


import com.google.common.collect.Maps;
import com.sc.security.beans.PageQuery;
import com.sc.security.beans.PageResult;
import com.sc.security.common.JsonData;
import com.sc.security.model.SysRole;
import com.sc.security.service.AclService;
import com.sc.security.service.RoleService;
import com.sc.security.vo.AclVo;
import org.omg.CORBA.PUBLIC_MEMBER;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/sys/acl")
public class AclController {

    @Autowired
    private AclService aclService;
    @Autowired
    private RoleService roleService;

    @RequestMapping("/save.json")
    @ResponseBody
    public JsonData saveDept(AclVo aclVo){
        aclService.save(aclVo);
        return JsonData.success();
    }
    @RequestMapping("/update.json")
    @ResponseBody
    public JsonData updateDept(AclVo aclVo){
        aclService.update(aclVo);
        return JsonData.success();
    }
    @RequestMapping("/delete.json")
    @ResponseBody
    public JsonData delteDept(@RequestParam("id") Integer id){
        aclService.delete(id);
        return JsonData.success();
    }

    @RequestMapping("/page.json")
    @ResponseBody
    public JsonData list(@RequestParam("aclModuleId")Integer aclModuleId, PageQuery pageQuery){
        PageResult pageResult = aclService.listUser(aclModuleId,pageQuery);
        return JsonData.success(pageResult);
    }

    @RequestMapping("/acls.json")
    @ResponseBody
    public JsonData acls(@RequestParam("aclId")Integer aclId){
        Map<String,Object> map = Maps.newHashMap();
        List<SysRole> roleList  = roleService.getRoleListByAclId(aclId);
        map.put("roles",roleList);
        map.put("users",roleService.getUserByRoleList(roleList));

        return JsonData.success(map);
    }
}
