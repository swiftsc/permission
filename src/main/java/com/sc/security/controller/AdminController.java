package com.sc.security.controller;

import com.sc.security.common.RequestHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

@Controller
@RequestMapping("/admin")
public class AdminController {

    @RequestMapping("/index.page")
    public ModelAndView index(Map model){

        model.put("username", RequestHolder.getCurrentUser().getUsername());
        return new ModelAndView("admin",model);
    }
}
