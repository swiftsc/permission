package com.sc.security.controller;


import com.sc.security.beans.LogService;
import com.sc.security.beans.PageQuery;
import com.sc.security.common.JsonData;
import com.sc.security.vo.LogVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/sys/log")
public class LogController {

    @Autowired
    private LogService logService;

    @RequestMapping("/log.page")
    public ModelAndView logPage(){
        return new ModelAndView("log");
    }


    @RequestMapping("/page.json")
    @ResponseBody
    public JsonData page(LogVo param, PageQuery page){
        return JsonData.success(logService.searchPageList(param, page));
    }

    @RequestMapping("/recover.json")
    @ResponseBody
    public JsonData recover(@RequestParam("id")int id){
        logService.recover(id);
        return JsonData.success();
    }
}
