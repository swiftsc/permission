package com.sc.security.controller;

import com.sc.security.common.JsonData;
import com.sc.security.dto.AclModuleDto;
import com.sc.security.service.AclModuleService;
import com.sc.security.service.TreeService;
import com.sc.security.vo.AclModuleVo;
import com.sc.security.vo.DeptVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("/sys/aclModule")
public class AclModuleController {

    @Autowired
    private AclModuleService aclModuleService;
    @Autowired
    private TreeService treeService;

    @RequestMapping("/acl.page")
    public ModelAndView page(){
        return new ModelAndView("acl");
    }
    @RequestMapping("/save.json")
    @ResponseBody
    public JsonData saveDept(AclModuleVo aclModuleVo){
        aclModuleService.save(aclModuleVo);
        return JsonData.success();
    }
    @RequestMapping("/update.json")
    @ResponseBody
    public JsonData updateDept(AclModuleVo aclModuleVo){
        aclModuleService.update(aclModuleVo);
        return JsonData.success();
    }
    @RequestMapping("/delete.json")
    @ResponseBody
    public JsonData delteDept(@RequestParam("id") Integer id){
        aclModuleService.delete(id);
        return JsonData.success();
    }
    @RequestMapping("/tree.json")
    @ResponseBody
    public JsonData treeAcl(){
        List<AclModuleDto> aclModuleDtos = treeService.aclTree();
        return JsonData.success(aclModuleDtos);
    }
//    @RequestMapping("/acls.json")
//    @ResponseBody
//    public JsonData acls(@RequestParam("id") Integer id){
//        aclModuleService.acls(id);
//        return JsonData.success();
//    }
}
