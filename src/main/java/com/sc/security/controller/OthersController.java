package com.sc.security.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class OthersController {

    @RequestMapping("/config/config.page")
    public ModelAndView config(){
        return new ModelAndView("config");
    }
}
