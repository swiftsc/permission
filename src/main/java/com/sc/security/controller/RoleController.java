package com.sc.security.controller;


import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.sc.security.common.JsonData;
import com.sc.security.dto.AclModuleDto;

import com.sc.security.model.SysUser;
import com.sc.security.service.*;
import com.sc.security.utils.StringUtil;
import com.sc.security.vo.RoleVo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/sys/role")
public class RoleController {

    @Autowired
    private RoleService roleService;
    @Autowired
    private  TreeService treeService;
    @Autowired
    private RoleAclService roleAclService;
    @Autowired
    private RoleUserService roleUserService;
    @Autowired
    private UserService userService;

    @RequestMapping("/role.page")
    public ModelAndView page(){
        return new ModelAndView("role");
    }
    @RequestMapping("/list.json")
    @ResponseBody
    public JsonData list(){
        List<RoleVo> list = roleService.list();
        return JsonData.success(list);
    }
    @RequestMapping("/save.json")
    @ResponseBody
    public JsonData save(RoleVo roleVo){
        roleService.save(roleVo);
        return JsonData.success();
    }

    @RequestMapping("/update.json")
    @ResponseBody
    public JsonData update(RoleVo roleVo){
        roleService.update(roleVo);
        return JsonData.success();
    }

    @RequestMapping("/roleTree.json")
    @ResponseBody
    public JsonData tree(@RequestParam("roleId")Integer id){
        List<AclModuleDto> aclModuleDtos = treeService.roleTree(id);
        return JsonData.success(aclModuleDtos);
    }

    @RequestMapping("/changeAcls.json")
    @ResponseBody
    public JsonData changeAcl(@RequestParam("roleId")Integer lastRoleId,@RequestParam(value = "aclIds",required = false,defaultValue = "")String aclIds){
        roleAclService.changeAcl(lastRoleId,aclIds);
        return JsonData.success();
    }
    @RequestMapping("/users.json")
    @ResponseBody
    public JsonData users(@RequestParam("roleId")Integer RoleId){
        List<SysUser> selectlist = roleUserService.getListByRoleId(RoleId);
        List<SysUser> allUser = userService.getAll();
        List<SysUser> unselect = Lists.newArrayList();

        Set<Integer> selectUserIdList = selectlist.stream().map(sysUser -> sysUser.getId()).collect(Collectors.toSet());
        for(SysUser user:allUser){
            if(user.getStatus()==1&&!selectUserIdList.contains(user.getId())){
                unselect.add(user);
            }
        }
        Map<String,List<SysUser>> map  = Maps.newHashMap();
        map.put("selected",selectlist);
        map.put("unselected",unselect);
        return JsonData.success(map);
    }

    @RequestMapping("/changeUsers.json")
    @ResponseBody
    public JsonData changeUsers(@RequestParam("roleId")Integer roleId,
                                @RequestParam(value = "userIds",required = false,defaultValue = "")String userIds){
        List<Integer> list = StringUtil.splitToListInt(userIds);
        roleUserService.changeRoleUsers(roleId,list);
        return JsonData.success();
    }
}
