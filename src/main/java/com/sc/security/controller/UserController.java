package com.sc.security.controller;


import com.google.common.collect.Maps;
import com.sc.security.beans.PageQuery;
import com.sc.security.beans.PageResult;
import com.sc.security.common.JsonData;
import com.sc.security.service.RoleService;
import com.sc.security.service.TreeService;
import com.sc.security.service.UserService;
import com.sc.security.vo.UserVo;
import org.omg.CORBA.PUBLIC_MEMBER;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

@Controller
@RequestMapping("/sys/user")
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private TreeService treeService;
    @Autowired
    private RoleService roleService;

    @RequestMapping("/save.json")
    @ResponseBody
    public JsonData addUser(UserVo userVo){
        userService.addUser(userVo);
        return  JsonData.success();
    }

    @RequestMapping("/update.json")
    @ResponseBody
    public JsonData updateUser(UserVo userVo){
        userService.updateUser(userVo);
        return  JsonData.success();
    }
    @RequestMapping("/page.json")
    @ResponseBody
    public JsonData listUser(@RequestParam("deptId")Integer deptId, PageQuery pageQuery){
           PageResult pageResult = userService.listUser(deptId,pageQuery);
           return JsonData.success(pageResult);
    }

    @RequestMapping("/acls.json")
    @ResponseBody
    public JsonData acls(@RequestParam("userId")Integer userId){
        Map<String,Object> map = Maps.newHashMap();
        map.put("acls",treeService.userAclTree(userId));
        map.put("roles",roleService.getRoleListByUserId(userId));
        return JsonData.success(map);
    }

    @RequestMapping("/noAuth.page")
    @ResponseBody
    public ModelAndView noAuth(){
        return new  ModelAndView("noAuth");
    }
}
