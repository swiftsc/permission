package com.sc.security.controller;



import com.sc.security.common.JsonData;
import com.sc.security.dto.DeptLevelDto;
import com.sc.security.service.DeptService;
import com.sc.security.service.TreeService;
import com.sc.security.vo.DeptVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/sys/dept")
public class DeptController {

    @Autowired
    private DeptService deptService;
    @Autowired
    private TreeService treeService;

    @RequestMapping("/save.json")
    @ResponseBody
    public JsonData saveDept(DeptVo deptVo){
            deptService.save(deptVo);
            return JsonData.success();
    }
    @RequestMapping("/dept.page")
    public ModelAndView page(){
        return new ModelAndView("dept");
    }
    @RequestMapping("/tree.json")
    @ResponseBody
    public JsonData tree(){
        List<DeptLevelDto> deptLevelDtos = treeService.deptTree();
        return JsonData.success(deptLevelDtos);
    }
    @RequestMapping("/update.json")
    @ResponseBody
    public JsonData updateDept(DeptVo deptVo){
        deptService.update(deptVo);
        return JsonData.success();
    }
    @RequestMapping("/delete.json")
    @ResponseBody
    public JsonData delteDept(@RequestParam("id") Integer id){
        deptService.delete(id);
        return JsonData.success();
    }
}
