package com.sc.security.service;

import com.sc.security.dto.AclModuleDto;
import com.sc.security.dto.DeptLevelDto;
import com.sc.security.dto.RoleDto;

import java.util.List;

public interface TreeService {
    List<DeptLevelDto> deptTree();
    List<AclModuleDto> aclTree();
    List<AclModuleDto> userAclTree(int userId);
    List<AclModuleDto> roleTree(Integer id);


}
