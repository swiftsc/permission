package com.sc.security.service;

import com.sc.security.model.SysAcl;

import java.util.List;

public interface SysCoreService {
    List<SysAcl> getCurrentUserAclList();
    List<SysAcl> getRoleAclList(int roleId);
    List<SysAcl> getUserAclList(int userId);
    boolean hasUrlAcl(String url);
}
