package com.sc.security.service;

import com.sc.security.beans.PageQuery;
import com.sc.security.beans.PageResult;
import com.sc.security.vo.AclVo;

public interface AclService {
    void save(AclVo aclVo);

    void update(AclVo aclVo);

    void delete(Integer id);

    PageResult listUser(Integer aclModuleId, PageQuery pageQuery);
}
