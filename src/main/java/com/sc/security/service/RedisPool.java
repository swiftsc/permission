package com.sc.security.service;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.ShardedJedisPool;

@Component("redisPool")
@Slf4j
public class RedisPool {

    @Autowired
    private ShardedJedisPool shardedJedisPool;

    public ShardedJedis instance(){
        return shardedJedisPool.getResource();
    }
    public void safeClose(ShardedJedis shardedJedis){

        try{
            if(shardedJedisPool!=null)
            {
                shardedJedisPool.close();
            }

        }catch (Exception e){
            log.error("redis关闭异常");
        }

    }
}
