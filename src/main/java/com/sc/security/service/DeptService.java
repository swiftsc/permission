package com.sc.security.service;

import com.sc.security.vo.DeptVo;

import javax.servlet.http.HttpServletRequest;

public interface DeptService {
    void save(DeptVo deptVo);

    void update(DeptVo deptVo);

    void delete(Integer id);
}
