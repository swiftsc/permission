package com.sc.security.service;


import com.sc.security.common.CacheKeyConstants;

public interface CacheService {
    void  saveCache(String value,int time,CacheKeyConstants prefix);
    void  saveCache(String value, int time, CacheKeyConstants prefix, String... keys);
    String getFromCache(CacheKeyConstants prefix,String... keys);
}
