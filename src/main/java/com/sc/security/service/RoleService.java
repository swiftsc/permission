package com.sc.security.service;

import com.google.common.collect.Lists;
import com.sc.security.dto.RoleDto;
import com.sc.security.model.SysRole;
import com.sc.security.model.SysUser;
import com.sc.security.vo.RoleVo;

import java.util.List;

public interface RoleService {
    List<RoleVo> list();

    void save(RoleVo roleVo);

    void update(RoleVo roleVo);

    List<SysRole> getRoleListByUserId(Integer userId);

    List<SysRole> getRoleListByAclId(Integer aclId);

    List<SysUser> getUserByRoleList(List<SysRole> roleList);
}
