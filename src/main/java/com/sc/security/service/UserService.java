package com.sc.security.service;

import com.sc.security.beans.PageQuery;
import com.sc.security.beans.PageResult;
import com.sc.security.model.SysUser;
import com.sc.security.vo.UserVo;

import java.util.List;

public interface UserService {
    void addUser(UserVo userVo);
    void updateUser(UserVo userVo);

    SysUser login(String username);
    List<SysUser> getAll();
    PageResult<SysUser> listUser(Integer deptId, PageQuery pageQuery);
}
