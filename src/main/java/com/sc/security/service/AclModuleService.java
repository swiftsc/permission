package com.sc.security.service;

import com.sc.security.vo.AclModuleVo;

public interface AclModuleService {
    void save(AclModuleVo aclModuleVo);

    void update(AclModuleVo aclModuleVo);

    void delete(Integer id);


    void acls(Integer id);
}
