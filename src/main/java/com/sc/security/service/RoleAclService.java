package com.sc.security.service;

public interface RoleAclService {

    void changeAcl(Integer lastRoleId, String aclIds);
}
