package com.sc.security.service;

import com.sc.security.vo.ModifyUserVo;

public interface ModifyUserInfoService {
    void change(ModifyUserVo userVo);
}
