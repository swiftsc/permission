package com.sc.security.service;

import com.sc.security.model.SysUser;

import java.util.List;

public interface RoleUserService {
    List<SysUser> getListByRoleId(Integer roleId);

    void changeRoleUsers(Integer roleId, List<Integer> list);
}
