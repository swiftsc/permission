package com.sc.security.service.impl;

import com.google.common.base.Preconditions;
import com.sc.security.beans.LogService;
import com.sc.security.beans.PageQuery;
import com.sc.security.beans.PageResult;
import com.sc.security.common.RequestHolder;
import com.sc.security.dao.SysAclMapper;
import com.sc.security.exception.ParamException;
import com.sc.security.model.SysAcl;
import com.sc.security.model.SysUser;
import com.sc.security.service.AclService;
import com.sc.security.utils.BeanValidator;
import com.sc.security.utils.IpUtil;
import com.sc.security.vo.AclVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class AclServiceImpl implements AclService {
    @Autowired
    private SysAclMapper aclMapper;
    @Autowired
    private LogService logService;

    @Override
    public void save(AclVo aclVo) {
        BeanValidator.check(aclVo);
        if (checkExist(aclVo.getAclModuleId(),aclVo.getName(),aclVo.getId())){
            throw new ParamException("当前权限模块下有相同的名称");
        }
        SysAcl sysAcl = new SysAcl();
        BeanUtils.copyProperties(aclVo,sysAcl);
        sysAcl.setOperator(RequestHolder.getCurrentUser().getUsername());
        sysAcl.setOperateIp(IpUtil.getUserIP(RequestHolder.getCurrentRequest()));
        sysAcl.setOperateTime(new Date());
        sysAcl.setCode(generateCode());
        sysAcl.setId(null);
        aclMapper.insertSelective(sysAcl);
        logService.saveAcl(null,sysAcl);
    }

    @Override
    public void update(AclVo aclVo) {
        BeanValidator.check(aclVo);
        SysAcl before = aclMapper.selectByPrimaryKey(aclVo.getId());
        Preconditions.checkNotNull(before,"信息存在");
        SysAcl after = new SysAcl();
        BeanUtils.copyProperties(aclVo,after);
        after.setOperateTime(new Date());
        after.setOperator(RequestHolder.getCurrentUser().getUsername());
        after.setCode(before.getCode());
        after.setOperateIp(IpUtil.getUserIP(RequestHolder.getCurrentRequest()));
        aclMapper.updateByPrimaryKey(after);
        logService.saveAcl(before,after);
    }

    @Override
    public void delete(Integer id) {

    }

    @Override
    public PageResult<SysAcl> listUser(Integer aclModuleId, PageQuery pageQuery) {
        BeanValidator.check(pageQuery);
        int count  = aclMapper.selectCountByModuleId(aclModuleId);
        if(count>0){
            List<SysAcl> userList = aclMapper.selectAclList(aclModuleId,pageQuery);
            PageResult pageResult = new PageResult();
            pageResult.setTotal(count);
            pageResult.setData(userList);
            return pageResult;
        }
        return new PageResult<>();
    }
    private boolean checkExist(Integer parentId,String aclName,Integer aclId){
        int count = aclMapper.countByNameAndParentId(parentId,aclName,aclId);
        if(count>0){
            return true;
        }
        return false;
    }
    public String generateCode(){
        SimpleDateFormat format=  new SimpleDateFormat("yyyyMMddHHmmSS");

        return format.format(new Date())+"_"+Math.random()*100;
    }

}
