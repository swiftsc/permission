package com.sc.security.service.impl;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.sc.security.beans.LogService;
import com.sc.security.common.RequestHolder;
import com.sc.security.dao.SysRoleAclMapper;
import com.sc.security.dao.SysRoleMapper;
import com.sc.security.dao.SysRoleUserMapper;
import com.sc.security.dao.SysUserMapper;
import com.sc.security.exception.ParamException;
import com.sc.security.model.SysRole;
import com.sc.security.model.SysUser;
import com.sc.security.service.RoleService;
import com.sc.security.utils.BeanValidator;
import com.sc.security.utils.IpUtil;
import com.sc.security.vo.RoleVo;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private SysRoleMapper roleMapper;
    @Autowired
    private SysRoleUserMapper roleUserMapper;
    @Autowired
    private SysUserMapper userMapper;
    @Autowired
    private SysRoleAclMapper roleAclMapper;
    @Autowired
    private LogService logService;
    @Override
    public List<RoleVo> list() {
        List<SysRole> listRole  = roleMapper.listRole();
        List<RoleVo> list = Lists.newArrayList();
        if(listRole!=null&&listRole.size()>0){
            for(SysRole r:listRole)
            {
                RoleVo roleVo = new RoleVo();
                BeanUtils.copyProperties(r,roleVo);
                list.add(roleVo);
            }
        }
        return list;
    }

    @Override
    public void save(RoleVo roleVo) {
        BeanValidator.check(roleVo);
        if (checkExist(roleVo.getName(),roleVo.getId())){
            throw new ParamException("角色已经存在");
        }
        SysRole sysRole = new SysRole();
        BeanUtils.copyProperties(roleVo,sysRole);
        sysRole.setOperator(RequestHolder.getCurrentUser().getUsername());
        sysRole.setOperateIp(IpUtil.getUserIP(RequestHolder.getCurrentRequest()));
        sysRole.setId(null);
        roleMapper.insertSelective(sysRole);
        logService.saveRole(null,sysRole);
    }


    @Override
    public void update(RoleVo roleVo) {
        BeanValidator.check(roleVo);
        if (checkExist(roleVo.getName(),roleVo.getId())){
            throw new ParamException("角色已经存在");
        }

        SysRole before = roleMapper.selectByPrimaryKey(roleVo.getId());
        Preconditions.checkNotNull(before,"待更新项目不存在");
        SysRole after = new SysRole();
        BeanUtils.copyProperties(roleVo,after);
        after.setOperator(RequestHolder.getCurrentUser().getUsername());
        after.setOperateIp(IpUtil.getUserIP(RequestHolder.getCurrentRequest()));
        after.setOperateTime(new Date());
        roleMapper.updateByPrimaryKey(after);
        logService.saveRole(before,after);
    }

    @Override
    public List<SysRole> getRoleListByUserId(Integer userId) {
        List<Integer> roleIdList  = roleUserMapper.getRoleIdListByUserId(userId);
        if(CollectionUtils.isEmpty(roleIdList)){
            return Lists.newArrayList();
        }
        return roleMapper.getByIdList(roleIdList);

    }

    @Override
    public List<SysRole> getRoleListByAclId(Integer aclId) {
        List<Integer> roleIdList = roleAclMapper.getRoleIdListByAclId(aclId);
        if(CollectionUtils.isEmpty(roleIdList)){
            return Lists.newArrayList();
        }
        return roleMapper.selectByIdList(roleIdList);
    }

    @Override
    public List<SysUser> getUserByRoleList(List<SysRole> roleList) {
        if(CollectionUtils.isEmpty(roleList)){
            return Lists.newArrayList();
        }
        List<Integer> idList = roleList.stream().map(sysRole -> sysRole.getId()).collect(Collectors.toList());
        List<Integer> userIdList = roleUserMapper.getUserIdListByRoleIdList(idList);
        if(CollectionUtils.isEmpty(userIdList)){
            return Lists.newArrayList();
        }

        return userMapper.getByUserId(userIdList);
    }

    private boolean checkExist(String name, Integer id) {
        int count = roleMapper.countByNameAndId(name,id);
        if(count>0){
            return true;
        }
        return false;
    }
}
