package com.sc.security.dao;

import com.sc.security.model.SysRoleUser;
import com.sc.security.model.SysUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysRoleUserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SysRoleUser record);

    int insertSelective(SysRoleUser record);

    SysRoleUser selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysRoleUser record);

    int updateByPrimaryKey(SysRoleUser record);

    List<Integer> getRoleIdListByUserId(@Param("userId") int userId);

    List<Integer> getUserIdListByRoleId(@Param("roleId") Integer roleId);

    List<Integer> getUserIdListByRoleIdList(@Param("idList") List<Integer> idList);

    void deleteByRoleId(@Param("roleId") Integer roleId);

    void batchInsert(@Param("roleUserList") List<SysRoleUser> roleUserList);
}