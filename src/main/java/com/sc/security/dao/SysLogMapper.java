package com.sc.security.dao;

import com.sc.security.beans.PageQuery;
import com.sc.security.dto.LogDto;
import com.sc.security.model.SysLog;
import com.sc.security.model.SysLogWithBLOBs;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysLogMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SysLogWithBLOBs record);

    int insertSelective(SysLogWithBLOBs record);

    SysLogWithBLOBs selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysLogWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(SysLogWithBLOBs record);

    int updateByPrimaryKey(SysLog record);

    int countBySearchDto(@Param("dto") LogDto logDto);

    List<SysLogWithBLOBs> getPageListBySearchDto(@Param("dto") LogDto logDto, @Param("page") PageQuery page);
}