package com.sc.security.dao;

import com.sc.security.model.SysAclModule;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysAclModuleMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SysAclModule record);

    int insertSelective(SysAclModule record);

    SysAclModule selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysAclModule record);

    int updateByPrimaryKey(SysAclModule record);

    int countByNameAndParentId(@Param("parentId") Integer parentID, @Param("name") String name, @Param("id") Integer id);

    List<SysAclModule> getAllAcl();

    List<SysAclModule> getChildDeptListByLevel(@Param("level")String level);

    void batchUpdateLevel(@Param("aclList") List<SysAclModule> list);
}