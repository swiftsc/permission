package com.sc.security.dao;

import com.sc.security.beans.PageQuery;
import com.sc.security.model.SysUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysUserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SysUser record);

    int insertSelective(SysUser record);

    SysUser selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysUser record);

    int updateByPrimaryKey(SysUser record);

    int checkEmailExist(@Param("email") String email,@Param("userId")Integer userId);

    int checkPhoneExist(@Param("phone") String phone,@Param("userId")Integer userId);

    SysUser login(@Param("username") String username);

    int selectCountByDeptId(@Param("deptId") Integer deptId);

    List<SysUser> selectUserList(@Param("deptId") Integer deptId, @Param("page")PageQuery pageQuery);

    List<SysUser> selectByIdList(@Param("List") List<Integer> idList);

    List<SysUser> getAll();

    List<SysUser> getByUserId(@Param("idList") List<Integer> userIdList);

    void deleteByDeptId(@Param("deptId") Integer id);
}