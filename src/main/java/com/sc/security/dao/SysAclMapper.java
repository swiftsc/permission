package com.sc.security.dao;

import com.google.common.collect.Lists;
import com.sc.security.beans.PageQuery;
import com.sc.security.model.SysAcl;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysAclMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SysAcl record);

    int insertSelective(SysAcl record);

    SysAcl selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysAcl record);

    int updateByPrimaryKey(SysAcl record);

    int selectCountByModuleId(@Param("id") Integer aclModuleId);

    List<SysAcl> selectAclList(@Param("moduleId") Integer aclModuleId, @Param("page") PageQuery pageQuery);

    int countByNameAndParentId(@Param("moduleId") Integer ModuleId, @Param("name") String aclName, @Param("id") Integer aclId);

    List<SysAcl> getAll();

    List<SysAcl> getByIdList(@Param("idList") List<Integer> idList);

    List<SysAcl> getByUrl(@Param("url") String url);
}