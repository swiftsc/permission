package com.sc.security.vo;


import lombok.Data;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Data
@ToString
public class DeptVo {

    private Integer id;
    @NotNull
    @Length(max = 15,min = 2,message = "部门名称需要在2-15长度之间")
    private String name;
    private Integer parentId=0;
    @NotNull(message = "顺序不能为空")
    private Integer seq;
    @Length(max = 150,message = "备注不能超过150个长度")
    private String remark;
}
