package com.sc.security.vo;

import lombok.Data;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

@Data
@ToString
public class AclVo {
    private Integer id;

    @NotBlank(message = "权限名称不能为空")
    private String name;
    @NotNull(message = "必须指定模块")
    private Integer aclModuleId;
    private String url;
    @NotNull
    private Integer type;
    @NotNull
    private Integer status;
    private Integer seq;
    private String remark;
}
