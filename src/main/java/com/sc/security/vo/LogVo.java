package com.sc.security.vo;


import lombok.Data;

@Data
public class LogVo {
    private Integer type;
    private String beforeSeg;
    private String afterSeg;
    private String operator;
    private String fromTime;//yyyy-MM-dd
    private String toTime;

}
