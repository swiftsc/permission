package com.sc.security.vo;

import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Data
@ToString
public class RoleVo {
    private Integer id;

    @NotNull(message = "名称不可以为空")
    private String name;

    @NotNull
    private Integer type=1;

    @NotNull
    private Integer status=1;

    private String remark;
}
