package com.sc.security.vo;

import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

@Data
@ToString
public class AclModuleVo {
    private Integer id;

    @NotNull(message = "名称不能为空")
    private String name;

    @NotNull(message = "上级模块必须选择")
    private Integer parentId=0;

    @NotNull(message = "顺序必填")
    private Integer seq;

    @NotNull
    private Integer status;

    private String remark;
}
