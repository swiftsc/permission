package com.sc.security.vo;

import lombok.Data;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
@ToString
public class UserVo {
    private Integer id;
    @NotNull(message = "用户名不能为空")
    @Length(max = 20,message = "用户名过长")
    private String username;
    @NotNull(message = "手机号码不能为空")
    @Pattern(regexp = "^((17[0-9])|(14[0-9])|(13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$",message = "手机号码格式错误")
    private String telephone;
    @NotNull(message = "邮箱不能为空")
    @Pattern(regexp = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$",message = "邮箱格式错误")
    private String mail;
    @NotNull(message = "部门ID不能为空")
    private Integer deptId;
    @NotNull(message = "必须指定用户状态")
    @Max(value = 2,message = "状态不合法")
    @Min(value = 0,message = "状态不合法")
    private Integer status;
    private String remark;
}
