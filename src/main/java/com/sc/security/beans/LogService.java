package com.sc.security.beans;

import com.sc.security.model.*;
import com.sc.security.vo.LogVo;

import java.util.List;

public interface LogService {
    void saveDeptLog(SysDept before,SysDept after);
    void saveUserLog(SysUser before,SysUser after);
    void saveModuleAcl(SysAclModule before,SysAclModule after);
    void saveAcl(SysAcl before,SysAcl after);
    void saveRole(SysRole before,SysRole after);
    void saveRoleAcl(int roleId, List<Integer> before,List<Integer> after);
    void saveRoleUser(int roleId, List<Integer> before,List<Integer> after);

    PageResult searchPageList(LogVo param, PageQuery page);

    void recover(int id);
}
