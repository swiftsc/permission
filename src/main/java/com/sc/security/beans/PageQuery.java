package com.sc.security.beans;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;

public class PageQuery {
    @Getter
    @Setter
    @Min(value = 1,message = "页码不合法")
    private Integer pageNo=1;
    @Getter
    @Setter
    @Min(value = 1,message = "参数不合法")
    private Integer pageSize=10;

    @Setter
    private Integer offset;

    public Integer getOffset() {
        return (pageNo-1)*pageSize;
    }
}
