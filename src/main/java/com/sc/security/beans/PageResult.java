package com.sc.security.beans;


import com.google.common.collect.Lists;
import lombok.*;

import java.util.List;

@Data
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PageResult<T> {

    private List<T> data = Lists.newArrayList();
    private int total;
}
