<%--
  Created by IntelliJ IDEA.
  User: sc
  Date: 2017/11/13
  Time: 上午11:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>首页</title>
    <jsp:include page="../../common/backend_common.jsp"/>

</head>
<body class="no-skin" youdao="bind" style="background: white">
<input id="gritter-light" checked="" type="checkbox" class="ace ace-switch ace-switch-7"/>
<div class="page-header">
    <h1>
        <a href="/admin/index.page">首页</a>
    </h1>
</div>
<div style="margin: 0 auto;text-align: center;">
    <div style="margin-top: 200px">
        <h1 style="color: #990000;font-weight: bold;font-style: italic">欢迎来到个人中心</h1>
    </div>

    <div style="padding: 100px 100px  10px;">

        <form class="form-horizontal" role="form" id="tt">
            <div class="form-group" hidden="hidden">
                <label for="id" class="col-sm-2 control-label">id</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="id"  value="${user.id}">
                </div>
            </div>
            <div class="form-group">
                <label for="username" class="col-sm-2 control-label">用户名</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="username"  value="${user.username}">
                </div>
            </div>
            <div class="form-group">
                <label for="password" class="col-sm-2 control-label">密码</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="password" value="${user.password}">
                </div>
            </div>
            <div class="form-group">
                <label for="phone" class="col-sm-2 control-label">手机号</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="phone" value="${user.telephone}">
                </div>
            </div>
            <div class="form-group">
                <label for="email" class="col-sm-2 control-label">邮箱</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="email" value="${user.mail}">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-1 col-sm-10">
                    <button type="submit" class="btn btn-default">修改信息</button>
                </div>
            </div>
        </form>

    </div>

</div>

<script type="application/javascript">
    $(function() {
        function updateUser(successCallback, failCallback) {
            $.ajax({
                url: "/sys/profile/modify.json",
                data: $(".tt").serializeArray(),
                type: 'POST',
                success: function(result) {
                    if (result.ret) {
                        window.location.href="/admin/index.page"
                    } else {
                        if (failCallback) {
                            failCallback(result);
                        }
                    }
                }
            })
        }
        });
</script>
</body>
</html>
