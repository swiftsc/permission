## 权限管理系统
基于RBAC模型实现的一套权限管理系统，简单来说就是一个用户拥有若干角色，没个角色拥有若干权限。
### 主要功能
1. 通过filter做权限拦截
2. 通过redis做部分权限缓存
3. 权限更新记录并且可以还原

### 部分效果展示
*  首页
![](https://ws3.sinaimg.cn/large/006tNc79gy1fln4ayxhpzj31fc0p6t8t.jpg)
* 用户管理
![](https://ws2.sinaimg.cn/large/006tNc79gy1fln4c4nq5bj31ff0m2dg8.jpg)
* 角色管理
![](https://ws1.sinaimg.cn/large/006tNc79gy1fln4dk6sq0j31fd0nbwf2.jpg)
![](https://ws1.sinaimg.cn/large/006tNc79gy1fln4dhua3tj31fh0nnglz.jpg)
* 权限管理
![](https://ws3.sinaimg.cn/large/006tNc79gy1fln4epw024j31fh0mqgm2.jpg)
* 权限更新记录
![](https://ws1.sinaimg.cn/large/006tNc79gy1fln4ff14mij31a20n8jsc.jpg)